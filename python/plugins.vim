
" YouCompleteMe: a code-completion engine for Vim
Plugin 'ycm-core/YouCompleteMe'

" XMLEdit.vim is really useful for editing XML and HTML files
Plugin 'sukima/xmledit'


" Detects django projects and set correct filetype
Plugin 'tweekmonster/django-plus.vim'


Plugin 'nvie/vim-flake8'
