set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'


" PHP Plguins
"so ~/.vim/php/plugins.vim

" Python Plguins
so ~/.vim/python/plugins.vim



" directory browser
Plugin 'tpope/vim-vinegar'


Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'mileszs/ack.vim'
Plugin 'skwp/greplace.vim'

Plugin 'tobyS/vmustache'


" Surround.vim is all about "surroundings":
"  parentheses, brackets, quotes, XML tags, and mor
Plugin 'tpope/vim-surround'


" prettier Formatter
Plugin 'sbdchd/neoformat'

"Plugin 'ervandew/supertab'



"vim-solidity to detect the solidity syntax
Plugin 'tomlion/vim-solidity'




Plugin 'SirVer/ultisnips'
"	 	Optional snippets
Plugin 'honza/vim-snippets'



" Powerline
" Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}



" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
