FROM node:12.4.0-alpine

############### Installing VIM, Git and curl
RUN apk add --no-cache python
RUN apk add --no-cache git
RUN apk add --no-cache curl
RUN apk add --no-cache vim

############## Cloning the repository
RUN git clone https://gitlab.com/MoathZ/Vim-Configurations ~/.vim

############# vim plugins installers
RUN git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

############# Installing the plugins
RUN vim +PluginInstall +qall
#RUN vim +PlugInstall +qall

WORKDIR /app

