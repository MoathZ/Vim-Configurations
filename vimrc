set nocompatible "We want the latest Vim settings/options.


set encoding=utf-8

let mapleader = ',' "The default leader is \ but a comma is much better.

"set backspace=indent,eol,start "Make backspace behave like every other editor."

" Genaral Configrations:

set number
set relativenumber
"set nonumber

set autoindent tabstop=4 softtabstop=4 expandtab shiftwidth=4 smarttab " The width of a TAB is set to 4.
set autowriteall     " Automatically write the file when switching buffers.
set complete=.,w,b,u " set auto complete setting
"     current buffer, any open windows, any loaded buffers, any unloaded buffers

set formatoptions+=j


if exists('$SUDO_USER')
    " Don't create root-owned files
    set nobackup
    set nowritebackup
else
    " Remove swap and backup files from your working directory
    set backupdir=~/.vim/backup,.,/tmp
    set directory=~/.vim/backup,.,/tmp
endif



set list                              " show whitespace
set listchars=nbsp:⦸                  " CIRCLED REVERSE SOLIDUS (U+29B8, UTF-8: E2 A6 B8)
set listchars+=tab:▷┅                 " WHITE RIGHT-POINTING TRIANGLE (U+25B7, UTF-8: E2 96 B7)

set listchars+=extends:»              " RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00BB, UTF-8: C2 BB)
set listchars+=precedes:«             " LEFT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00AB, UTF-8: C2 AB)
set listchars+=trail:•                " BULLET (U+2022, UTF-8: E2 80 A2)




so ~/.vim/plugins.vim
so ~/.vim/plugs.vim

" PHP configurations
"so ~/.vim/php/vimrc

" ASP.NET configurations
"so ~/.vim/asp/vimrc

" Python configurations
so ~/.vim/python/vimrc





filetype plugin on
set path+=**
set wildmenu

noremap <leader>f :find<space>

"---------------------- Visuals ----------------------"
syntax enable
"colorscheme atom-dark-256
"colorscheme atom-dark-256
set t_Co=256
"colorscheme yaflandia   "C++ / C colors
colorscheme rdark-terminal2
"colorscheme jummidark



set guioptions-=e "We don't want Gui tab.

" to remove the side par from the gui vim
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R

"fake a custome left padding for each window
set foldcolumn=2
highlight FoldColumn ctermbg=NONE
" set the line number color to NONE
highlight LineNr ctermbg=NONE
" remove the color of the Vertical split
highlight VertSplit ctermbg=NONE ctermfg=NONE





"---------------------- Mappings ----------------------"
" imap is a map for insert mod
" nmap is a map for normal mod
nmap <Leader>Q :bufdo bd<cr>
nmap <Leader>q :bd<cr>

" Ctrl-E Escape
vmap <C-E> <Esc>
imap <C-E> <Esc>

" Create A New File In A New Directory after doing :e newDir/file.new
nmap <Leader>nd  :!mkdir<space>-p<space>%:h<cr>
" ******************************************************
" :echo $MYVIMRC


" Make it easy to edit the vimrc file.
nmap <Leader>ev :tab drop $MYVIMRC<cr>
" Make it easy to edi the python vimrc file.
nmap <Leader>epv :tab drop ~/.vim/python/vimrc<cr>
"nmap <Leader>es :CtrlP ~/.vim/snippets/<cr>



" Add Simple Highlight Removal.
nmap <Leader><space> :nohlsearch<cr>


" Tage Find (tf)
nmap <Leader>tf :tag<space>
" go to the next tag, Tage Next (tn)
nmap <Leader>tn :tn<cr>
" go to the previous tag, Tage Previous (tp)
nmap <Leader>tp :tp<cr>
" select from all the available tags, Tage Select (ts)
nmap <Leader>ts :ts<cr>




"---------------------- Plugins ----------------------"

"/
"/ CtrlP
"/

set wildignore+=*/tmp/*,*.so,*.swp,*.zip
"let g:ctrlp_by_filename=1
let g:ctrlp_custom_ignore = {
  \ 'dir':  'vendor\|laradock\|node_modules\|\v[\/]\.git',
  \ 'file': '\v\.(exe|so|dll)$', }
" \ 'link': 'some_bad_symbolic_links',
" \ }

let g:ctrlp_match_window = 'top,order:ttb,min:1,max:15,results:30'



"/
"/ NERDTree
"/

let NERDTreeHijackNetrw = 0
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

nmap <Leader>` :NERDTreeToggle<cr>



"/
"/ Greplace.vim
"/
set grepprg=ag

let g:grep_cmd_opts = '--line-numbers --noheading'



"/
"/ The Silver Searcher & Ack.vim
"/
" Editor Integration Ag with ack.vim
let g:ackprg = 'ag --nogroup --nocolor --column'




"/
"/ Prettier, a code formatting
"/
"nmap <Leader>pr <Plug>(Prettier)
""Disable auto formatting of files that have "@format" tag
"let g:prettier#autoformat = 0
"" Running before saving async
""autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
"
"" force prettier to async
"let g:prettier#exec_cmd_async = 1
"
"" number of spaces per indentation level
"let g:prettier#config#tab_width = 4
"
"" single quotes over double quotes
"let g:prettier#config#single_quote = 'true'
"
"" print spaces between brackets
"let g:prettier#config#bracket_spacing = 'true'
"
"" put > on the last line instead of new line
"let g:prettier#config#jsx_bracket_same_line = 'true'





"/
"/ ultisnips
"/
let g:UltiSnipsSnippetDirectories  = ['~/.vim/UltiSnips', 'UltiSnips']
let g:UltiSnipsExpandTrigger       = "<c-j>"
let g:UltiSnipsJumpForwardTrigger  = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsListSnippets        = "<c-s>" "List possible snippets based on current file
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"





"---------------------- Auto-Commands  ----------------------"
augroup autosourcing
    autocmd!
    "Automatically sourve the vimrc file on save
    autocmd BufWritePost ~/.vimrc source %
    autocmd BufWritePost ~/.vim/vimrc source %
    autocmd BufWritePost ~/.vim/python/vimrc source %
    autocmd BufWritePost ~/.vim/plugins.vim source %
    autocmd BufWritePost ~/.vim/plugs.vim source %
augroup END





"---------------------- Searcing  ----------------------"
set hlsearch
set incsearch





"---------------------- Split Management  ----------------------"
set splitbelow
set splitright
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>





"---------------------- Tabs  ----------------------"
" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>


