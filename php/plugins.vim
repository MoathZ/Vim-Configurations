
" 			Vim syntax for PHP.
Plugin 'StanAngeloff/php.vim'

"           PHP Documentor
Plugin 'tobyS/pdv'


" 		For inserting "use" statements automatically.
Plugin 'arnaud-lb/vim-php-namespace'



"PSR2 Formatting
Plugin 'stephpy/vim-php-cs-fixer'
